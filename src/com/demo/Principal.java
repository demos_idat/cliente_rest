package com.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Principal {
	
	public static final String URL_BASE = "http://localhost:8080/restjee/rest/personas";

	public static void main(String[] args) {
		
		
		List<Persona> lista =  listar();
		for (Persona persona : lista) {
			System.out.println(persona.getNombre());
		}
		
		Persona persona = buscar(2);
		System.out.println(persona.getEmail());
		
		
		Persona _p = new Persona(0, "Joel", "Sacnhez");
		_p = agregar(_p);
		System.out.println(_p.getId());

	}
	
	public static List<Persona> listar(){
		List<Persona> lista = new ArrayList<>();
		try {
			URL url = new URL(URL_BASE);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuilder respuesta = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				respuesta.append(output);
			}
			Gson g = new Gson();
			lista = g.fromJson(respuesta.toString(), new TypeToken<List<Persona>>(){}.getType());
			conn.disconnect();
		} catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		return lista;
	}
	
	public static Persona buscar(int id) {
		Persona persona = new Persona();
		try {
			URL url = new URL(URL_BASE + "/" + id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuilder respuesta = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				respuesta.append(output);
			}
			Gson g = new Gson();
			persona = g.fromJson(respuesta.toString(), Persona.class);
			conn.disconnect();
		} catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		return persona;
	}
	
	public static Persona agregar(Persona persona) {
		try {
			URL url = new URL(URL_BASE);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			Gson g = new Gson();
			String input = g.toJson(persona, Persona.class);

			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {//HttpURLConnection.HTTP_CREATED
				throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			StringBuilder respuesta = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				respuesta.append(output);
			}
			Gson _g = new Gson();
			persona = _g.fromJson(respuesta.toString(), Persona.class);

			conn.disconnect();
		}  catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		return persona;
	}

}
